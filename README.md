# A11yWorks / Accessibility Works

Repository of documents and test files related to accessibility, especially web accessibility.

## Licence and Copyright

Copyright: Christophe Strobbe, 2015 – 2022.

Licence for the content: Creative Commons Attribution Share-Alike [CC BY-SA 4.0](LICENCE.html).

### Licence and Copyright for Other Resources

The JavaScript file [wcag-parsing-bookmarklet.js](tools/bookmarklets/wcag-parsing-bookmarklet.js) was adapted from
[Steve Faulkner's WCAG parsing only bookmarklet](https://github.com/stevefaulkner/wcagparsing).
Both Steve Faulkner's version and my derived version are available under the terms of the GNU General Public License version 2.

