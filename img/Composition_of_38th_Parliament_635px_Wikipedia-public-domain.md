# File:Composition of 38th Parliament.png

Source: [Wikipedia](https://en.wikipedia.org/wiki/File:Composition_of_38th_Parliament.png).

According to Wikipedia, this chart is in the public domain.

Chart used in [Wikipedia article "Chart"](https://en.wikipedia.org/wiki/Chart) (accessed on 05.03.2023).
