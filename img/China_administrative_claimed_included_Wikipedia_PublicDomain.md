# China_administrative_claimed_included.svg

Original URL: https://upload.wikimedia.org/wikipedia/commons/d/d2/China_administrative_claimed_included.svg

Wikimedia Commons page: https://commons.wikimedia.org/wiki/File:China_administrative_claimed_included.svg

(Size of this PNG preview of this SVG file: 735 × 599 pixels. Other resolutions: 294 × 240 pixels | 589 × 480 pixels | 942 × 768 pixels | 1,256 × 1,024 pixels | 2,511 × 2,048 pixels | 857 × 699 pixels.)

This file is in the public domain.

Wikipedia link: https://en.wikipedia.org/wiki/Provinces_of_China#/media/File:China_administrative_claimed_included.svg

Wikipedia article where the map is used:
[Provinces of China](https://en.wikipedia.org/wiki/Provinces_of_China). 
