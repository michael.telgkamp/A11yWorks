# Wikipedia logo

Source: [File:Wikipedia-logo-v2.svg](https://en.wikipedia.org/wiki/File:Wikipedia-logo-v2.svg) on Wikpedia.

## Licence

* This work is licensed under the [Creative Commons Attribution-ShareAlike 3.0 License](https://creativecommons.org/licenses/by-sa/3.0/).
* This file is (or includes) one of the official logos or designs used by the Wikimedia Foundation or by one of its projects. Use of the Wikimedia logos and trademarks is subject to the Wikimedia [trademark policy](https://foundation.wikimedia.org/wiki/Trademark_policy) and [visual identity guidelines](https://foundation.wikimedia.org/wiki/Wikimedia_visual_identity_guidelines), and may require permission.
