/*combined-select-1.js*/
/*jslint
    this
    for
*/

let DEBUG = false;

/**
 * @namespace The main application object.
 */
let combinedSelect = {
  init: function() {
    "use strict";
    // Check support for W3C DOM. (C. Heilmann: Beginning JavaScript Development ..., p. 66-68.)
    if (!document.getElementById || !document.createTextNode) { return; }

    this.addFirstChoiceListener();
  },

  addFirstChoiceListener:function() {
    "use strict";
    addListener(document.getElementById("category"), "change", combinedSelect.handleChoiceToggle);
  },

  handleChoiceToggle:function(event) {
    "use strict";
    // @@TODO replace with DOMhelp.stopDefault(event) and DOMhelp.stopBubble(event) (?)
    event.preventDefault();
    event.stopPropagation();
    combinedSelect.toggleSecondarySelectList(event.target, event.type);
  },

  toggleSecondarySelectList:function(target, type) {
    "use strict";
    let category = target.options[target.selectedIndex].value;
    if (DEBUG) { console.log("Category = " + category); }

    let containers = document.getElementsByClassName("second-choice");
    for (let i = 0; i < containers.length; i++) {
      if (!containers[i].classList.contains("hidden")) {
        containers[i].classList.add("hidden");
      }
    }
    // Dirty trick so I don't need to traverse the DOM to find the select element's parent:
    let container = document.getElementById(category + "-container");
    container.classList.remove("hidden");
  }
};


addLoadEvent(function() {
    "use strict";
    // Check support for W3C DOM. (C. Heilmann: Beginning JavaScript Development ..., p. 66-68.)
    if (!document.getElementById || !document.createTextNode) {return;}
    combinedSelect.init();
});


