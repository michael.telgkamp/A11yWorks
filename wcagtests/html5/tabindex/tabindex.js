/*tabindex.js*/
/*jslint
    this
    for
*/

let DEBUG = false;

/**
 * @namespace The main application object.
 */

let tabindexTest = {
  
  enterKey : 13,
  spaceKey : 32,

  init: function() {
    "use strict";
    // Check support for W3C DOM. (C. Heilmann: Beginning JavaScript Development ..., p. 66-68.)
    if (!document.getElementById || !document.createTextNode) { return; }

    this.addFirstChoiceListener();
  },

  addFirstChoiceListener:function() {
    "use strict";
    addListener(document.getElementById("test-misaligned-keyboard-access-1"), "keyup", tabindexTest.handleKeyUp);
  },

  handleKeyUp:function(event) {
    "use strict";
    // @@TODO replace with DOMhelp.stopDefault(event) and DOMhelp.stopBubble(event) (?)
    event.preventDefault();
    event.stopPropagation();
    tabindexTest.doSomethingOnKeyUp(event.target, event);
  },

  doSomethingOnKeyUp:function(target, event) {
    "use strict";
    if (event.type === "keyup" && event.isComposing === false) {
      if (event.keyCode === tabindexTest.enterKey) {
        console.log("Enter.");
      } else if (event.keyCode === tabindexTest.spaceKey) {
        console.log("Space.");
      } else {
        console.log("Neither Space nor Enter.");
      }
    } else if (event.type === "click" || event.type === "mousedown" || event.type === "mouseup")  {
      console.log("Mouse event.");
    } else {
      console.log("Other type of event");
    }
  }
};


addLoadEvent(function() {
    "use strict";
    // Check support for W3C DOM. (C. Heilmann: Beginning JavaScript Development ..., p. 66-68.)
    if (!document.getElementById || !document.createTextNode) {return;}
    tabindexTest.init();
});


