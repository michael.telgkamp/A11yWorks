/*modal-dialog.js*/
/*jslint
    this
    for
*/

let DEBUG = false;

/**
 * @namespace The main application object.
 */
let modalDialog = {
  init: function() {
    "use strict";
    // Check support for W3C DOM. (C. Heilmann: Beginning JavaScript Development ..., p. 66-68.)
    if (!document.getElementById || !document.createTextNode) { return; }

    this.addCookieButtonListener();
  },

  previousActiveElement: null,

  addCookieButtonListener:function() {
    "use strict";
    document.getElementById('cookie-opener').addEventListener('click', modalDialog.handleCookieConsentRequest);
  },

  handleCookieConsentRequest:function() {
    "use strict";
    modalDialog.openDialog();
  },

  openDialog:function() {
    "use strict";
    // Store reference to previously active element
    modalDialog.previousActiveElement = document.ActiveElement;

    if (modalDialog.previousActiveElement === null || modalDialog.previousActiveElement === undefined) {
      modalDialog.previousActiveElement = document.getElementById('cookie-opener');
    }

    // Make dialog visible
    window.confirm('Do you accept cookies?');
  },

  closeDialog:function() {
    "use strict";

    // Restore focus to previously active element
    // Without this, focus moves back to the top of the page
    modalDialog.previousActiveElement.focus({focusVisible: true});
  }

};


addLoadEvent(function() {
    "use strict";
    // Check support for W3C DOM. (C. Heilmann: Beginning JavaScript Development ..., p. 66-68.)
    if (!document.getElementById || !document.createTextNode) {return;}
    modalDialog.init();
});

/**
 * Add a function to the list of functions that need to be called when
 * the HTML document has finished loading (in other words, after the <code>window.load</code> event).
 * @param {Function} func A function that needs to be invoked after <code>window.load</code>.
 * @static
 * @author Simon Willison
 * @see Simon Willison's article <a href="https://simonwillison.net/2004/May/26/addLoadEvent/">Executing JavaScript on page load</a> (26 May 2004; last acccessed on 09.08.2022).
 */
function addLoadEvent(func) {
    "use strict";
    var oldonload = window.onload;
    if (typeof window.onload !== 'function') {
        window.onload = func;
    } else {
        window.onload = function() {
            if (oldonload) {
                oldonload();
            }
            func();
        };
    }
}

