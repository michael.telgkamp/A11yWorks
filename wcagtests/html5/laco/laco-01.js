/*modal-dialog.js*/
/*jslint
    this
    for
*/

let DEBUG = false;
const KEYCODE = {
  ESC: 27
};

/**
 * @namespace The main application object.
 */
let modalDialog = {
  init: function() {
    "use strict";
    // Check support for W3C DOM. (C. Heilmann: Beginning JavaScript Development ..., p. 66-68.)
    if (!document.getElementById || !document.createTextNode) { return; }

    this.addLacoButtonListener();
  },

  dialog: document.querySelector('.modal'),
  dialogMask: document.querySelector('.dialog__mask'),
  dialogWindow: document.querySelector('.dialog__window'),// instead of dialog.querySelector('.dialog__window')
  previousActiveElement: null,

  checkCloseDialog:function(e) {
    "use strict";
    if (e.keyCode === KEYCODE.ESC) {
      modalDialog.closeDialog();
    }
  },

  addLacoButtonListener:function() {
    "use strict";
    document.querySelectorAll('.wtLaco').forEach(function(item) {
        item.addEventListener('click', modalDialog.handleLacoRequest);
      }
    );
  },

  handleLacoRequest:function(e) {
    "use strict";
    let source = e.target || e.srcElement;
    modalDialog.openDialog(source);
  },

  openDialog:function(source) {
    "use strict";
    // Store reference to previously active element
    modalDialog.previousActiveElement = source;
    
    // Make all siblings of the dialog inert
    // Assumes that dialog is sibling of main etc, and child of body
    Array.from(document.body.children).forEach(child => {
      if (child !== modalDialog.dialog) {
        //In January 2023, this still required the inert polyfill for Firefox 102.6.0esr
        child.inert = true;
      }
    });

    // Make dialog visible
    modalDialog.dialog.classList.add('opened');

    // Listen for events that should close the dialog
    modalDialog.dialogMask.addEventListener('click', modalDialog.closeDialog);
    modalDialog.dialogWindow.querySelectorAll('button').forEach(btn => {
      btn.addEventListener('click', modalDialog.closeDialog);
    });
    document.addEventListener('keydown', modalDialog.checkCloseDialog);

    // Move focus into the dialog
    modalDialog.dialog.querySelector('#modal-dialog-label').focus();
  },

  closeDialog:function() {
    "use strict";
    modalDialog.dialogMask.removeEventListener('click', modalDialog.closeDialog);
    modalDialog.dialogWindow.querySelectorAll('button').forEach(btn => {
      btn.removeEventListener('click', modalDialog.closeDialog);
    });
    document.removeEventListener('click', modalDialog.closeDialog);

    Array.from(document.body.children).forEach(child => {
      if (child !== modalDialog.dialog) {
        child.inert = false;
      }
    });

    // Hide the dialog window
    modalDialog.dialog.classList.remove('opened');

    // Restore focus to previously active element
    // Without this, focus moves back to the top of the page
    modalDialog.previousActiveElement.focus({focusVisible: true});
  }

};


addLoadEvent(function() {
    "use strict";
    // Check support for W3C DOM. (C. Heilmann: Beginning JavaScript Development ..., p. 66-68.)
    if (!document.getElementById || !document.createTextNode) {return;}
    modalDialog.init();
});

/**
 * Add a function to the list of functions that need to be called when
 * the HTML document has finished loading (in other words, after the <code>window.load</code> event).
 * @param {Function} func A function that needs to be invoked after <code>window.load</code>.
 * @static
 * @author Simon Willison
 * @see Simon Willison's article <a href="https://simonwillison.net/2004/May/26/addLoadEvent/">Executing JavaScript on page load</a> (26 May 2004; last acccessed on 09.08.2022).
 */
function addLoadEvent(func) {
    "use strict";
    var oldonload = window.onload;
    if (typeof window.onload !== 'function') {
        window.onload = func;
    } else {
        window.onload = function() {
            if (oldonload) {
                oldonload();
            }
            func();
        };
    }
}
